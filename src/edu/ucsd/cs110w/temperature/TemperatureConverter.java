/**
 * 
 */
package edu.ucsd.cs110w.temperature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * TODO (cs15whc): write class javadoc
 *
 * @author cs15whc
 *
 */
public class TemperatureConverter {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException
	{
		String input = null;
		Temperature inputTemp = null, outputTemp = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true)
		{
			System.out.println("\nAvailable units: K, C, F");
			System.out.print("Enter temperature to convert (i.e. 36.8 C, 451 F, 273.1 K): ");
			if((input = reader.readLine()) == null) System.exit(0);
			String[] temp_in = input.split(" ");
			float temp_val = Float.parseFloat(temp_in[0]);
			switch(temp_in[1].toLowerCase().charAt(0))
			{
			case 'k':
				inputTemp = new Kelvin(temp_val);
				break;
			case 'c':
				inputTemp = new Celsius(temp_val);
				break;
			case 'f':
				inputTemp = new Fahrenheit(temp_val);
				break;
			default:
				System.out.println("Invalid entry!!\n\n");
				continue;
			}
			System.out.print("Enter the unit to convert TO: ");
			if((input = reader.readLine()) == null) System.exit(0);
			switch(input.toLowerCase().charAt(0))
			{
			case 'k':
				outputTemp = inputTemp.toKelvin();
				break;
			case 'c':
				outputTemp = inputTemp.toCelsius();
				break;
			case 'f':
				outputTemp = inputTemp.toFahrenheit();
				break;
			default:
				System.out.println("Invalid entry!!\n\n");
				continue;
			}
			System.out.println("\nThe converted temperature is " + 
								outputTemp.toString() + "\n\n");			
		}
	}
}
