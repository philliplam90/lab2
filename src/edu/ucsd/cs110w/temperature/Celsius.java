/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs15whc
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return this.getValue()+" C";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius(this.getValue());
	}
	@Override
	public Temperature toFahrenheit() {
		float C = 9.0f/5.0f*this.getValue()+32;
		return new Fahrenheit(C);
	}
	public Temperature toKelvin() {
		float K = this.getValue()+273.15f;
		return new Kelvin(K);
	}
}


