/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs15whc
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		//TODO Complete this method
		return this.getValue() + " F";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		float C = 5.0f/9.0f*(this.getValue()-32.0f);
		return new Celsius(C);
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		return new Fahrenheit(this.getValue());
	}
	public Temperature toKelvin() {
		float K = 5.0f/9.0f*(this.getValue()-32.0f)+273.15f;
		return new Kelvin(K);
	}
}
